package math

import "testing"

func TestEpsilon_Cmp(t *testing.T) {
	if z := Epsilon(1e-6).Cmp(3.1415926, 3.1415927); z != 0 {
		t.Errorf("Epsilon(1e-6).Cmp(3.1415926, 3.1415927) = %d, want 0", z)
	}
	if z := Epsilon(1e-9).Cmp(3.1415926, 3.1415927); z != -1 {
		t.Errorf("Epsilon(1e-6).Cmp(3.1415926, 3.1415927) = %d, want -1", z)
	}
	if z := Epsilon(1e-9).Cmp(3.1415927, 3.1415926); z != 1 {
		t.Errorf("Epsilon(1e-6).Cmp(3.1415926, 3.1415927) = %d, want 1", z)
	}
}

package big

import (
	"testing"
	"math/big"
)

func TestEpsilon_Cmp(t *testing.T) {
	if z := (*Epsilon)(big.NewFloat(1e-6)).Cmp(big.NewFloat(3.1415926), big.NewFloat(3.1415927)); z != 0 {
		t.Errorf("Epsilon(1e-6).Cmp(3.1415926, 3.1415927) = %d, want 0", z)
	}
	if z := (*Epsilon)(big.NewFloat(1e-9)).Cmp(big.NewFloat(3.1415926), big.NewFloat(3.1415927)); z != -1 {
		t.Errorf("Epsilon(1e-6).Cmp(3.1415926, 3.1415927) = %d, want -1", z)
	}
	if z := (*Epsilon)(big.NewFloat(1e-9)).Cmp(big.NewFloat(3.1415927), big.NewFloat(3.1415926)); z != 1 {
		t.Errorf("Epsilon(1e-6).Cmp(3.1415926, 3.1415927) = %d, want 1", z)
	}
}

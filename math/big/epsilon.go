package big

import "math/big"

type Epsilon big.Float

func (e *Epsilon) Cmp(x, y *big.Float) int {
	switch z := (&big.Float{}).Sub(x, y); {
	case (&big.Float{}).Neg((*big.Float)(e)).Cmp(z) < 0 && z.Cmp((*big.Float)(e)) < 0:
		return 0

	case z.Cmp(big.NewFloat(0)) < 0:
		return -1

	default:
		return 1
	}
}

package math

type Epsilon float64

func (e Epsilon) Cmp(x, y float64) int {
	switch z := x - y; {
	case -float64(e) < z && z < float64(e):
		return 0

	case z < 0:
		return -1

	default:
		return 1
	}
}
